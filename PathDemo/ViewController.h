//
//  ViewController.h
//  PathDemo
//
//  Created by 劳中成 on 12-3-17.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageButton.h"
#import "MaskView.h"
#import "HistoryViewController.h"

@interface ViewController : UIViewController<ImageButtonDelegate, UIScrollViewDelegate> {
    NSArray *_storyButtons;
    MaskView *_maskView;
    HistoryViewController *_historyViewController;
}

@property (retain, nonatomic) IBOutlet ImageButton *requestButton;
@property (retain, nonatomic) IBOutlet UIBarButtonItem *configItem;
@property (retain, nonatomic) IBOutlet UIBarButtonItem *connectionItem;

- (IBAction)pushConfigView:(id)sender;
- (IBAction)pushConnectionView:(id)sender;

@end
