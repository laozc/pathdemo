//
//  BackgroundImageView.h
//  PathDemo
//
//  Created by 劳中成 on 12-3-17.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#include <QuartzCore/QuartzCore.h>

@interface BackgroundImageView : UIView {
    UIImage *_image;
    CALayer *_backgroundImageLayer;
    CALayer *_contentLayer;
    CALayer *_buttonLayer;
    CAGradientLayer *_imageGradientLayer;
    CGSize boundsSize;
    BOOL _disableLayouting;
}

@property (retain, nonatomic) UIImage *backgroundImage;

- (void)scroll:(UIScrollView *)scrollView;

@end
