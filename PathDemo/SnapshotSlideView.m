//
//  SideAttachedView.m
//  PathDemo
//
//  Created by 劳中成 on 12-3-18.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "SnapshotSlideView.h"

@interface SlideView : UIView {
@private
    CALayer *_snapshotLayer;
    UIView *_snapshotView;
    CGPoint _panLocation;
    CGPoint _slidePosition;
    CGFloat _panOffset;
    BOOL _direction;
}

- (void)animateSlideIn;
- (void)animateSlideOut;

@end

@implementation SlideView

- (void)handleTap:(UIGestureRecognizer *)sender {
    if ([sender state] != UIGestureRecognizerStateEnded) {
        return;
    }
    
    CGPoint pt = [sender locationInView:self];
    if ([_snapshotLayer hitTest:pt] != nil) {
        [self animateSlideIn];
    }
}

- (void)handlePan:(UIGestureRecognizer *)sender {
    CGPoint pt = [sender locationInView:self];
    if ([sender state] == UIGestureRecognizerStateBegan) {
        _panLocation = pt;
        _panOffset = 0.0f;
    }
    
    if ([sender state] == UIGestureRecognizerStateEnded) {
        if (_direction) {
            if (_panOffset > 0) {
                [self animateSlideOut];
            } else {
                [self animateSlideIn];
            }
        } else {
            if (_panOffset > 0) {
                [self animateSlideIn];
            } else {
                [self animateSlideOut];
            }
        }
        
    } else if ([sender state] == UIGestureRecognizerStateChanged) {
        _panOffset = pt.x - _panLocation.x;
        [CATransaction begin];
        [CATransaction setDisableActions:YES];
        _snapshotLayer.position = CGPointMake(_snapshotLayer.position.x + _panOffset,
                                              _snapshotLayer.position.y);
        [CATransaction commit];
        _panLocation = pt;
    }
}

- (void)setupLayers {
    _snapshotLayer = [[CALayer layer] retain];
//    _snapshotLayer.backgroundColor = [[UIColor blueColor] CGColor];
    _snapshotLayer.bounds = _snapshotView.bounds;
    _snapshotLayer.anchorPoint = CGPointMake(0, 0);
    _snapshotLayer.position = CGPointMake(0,
                                          -ceilf(_snapshotView.layer.position.y - _snapshotView.layer.anchorPoint.y *  _snapshotView.layer.bounds.size.height));
    _snapshotLayer.shadowColor = [[UIColor blackColor] CGColor];
    _snapshotLayer.shadowRadius = 10;
    _snapshotLayer.shadowOpacity = 1.0f;
    
    [_snapshotLayer addSublayer:_snapshotView.layer];
    [self.layer addSublayer:_snapshotLayer];

    UITapGestureRecognizer *tapRecognizer = [[[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                     action:@selector(handleTap:)] autorelease];
    [self addGestureRecognizer:tapRecognizer];
}

- (id)initWithFrame:(CGRect)frame
    snapshotForView:(UIView *)view
  withSlidePosition:(CGPoint)pos
      slideFromLeft:(BOOL)direction
{
    self = [super initWithFrame:frame];
    if (self) {
        _snapshotView = [view retain];
        _slidePosition = pos;
        _direction = direction;
        [self setupLayers];
    }
    return self;
}

- (void)dealloc {
    [_snapshotView.layer removeFromSuperlayer];
    [_snapshotLayer release];
    [super dealloc];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupLayers];
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag {
    if (!flag) {
        return;
    }
    
    [[_snapshotView.layer retain] removeFromSuperlayer];
    [_snapshotLayer removeFromSuperlayer];
    [self.superview.superview addSubview:_snapshotView];
}

- (void)animateSlideOut {
    CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:@"position"];
    CGPoint destPos = CGPointMake(_slidePosition.x, _snapshotLayer.position.y);
    anim.fromValue = [NSValue valueWithCGPoint:_snapshotLayer.position];
    anim.toValue = [NSValue valueWithCGPoint:destPos];
    _snapshotLayer.position = destPos;
//    anim.delegate = self;
    [_snapshotLayer addAnimation:anim forKey:@"slideIn"];
}

- (void)animateSlideIn {
    CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:@"position"];
    CGPoint destPos = CGPointMake(0, _snapshotLayer.position.y);
    anim.fromValue = [NSValue valueWithCGPoint:_snapshotLayer.position];
    anim.toValue = [NSValue valueWithCGPoint:destPos];
    _snapshotLayer.position = destPos;
    anim.delegate = self;
    [_snapshotLayer addAnimation:anim forKey:@"slideOut"];
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    if ([_snapshotLayer hitTest:point]) {
        return self;
    }
    return nil;
}

@end

#pragma section SideAttachedView

@implementation SnapshotSlideView

- (void)handlePan:(UIGestureRecognizer *)sender {
    [_slideView handlePan:sender];
}

- (void)setupLayers {
    UIPanGestureRecognizer *panRecognizer = [[[UIPanGestureRecognizer alloc] initWithTarget:self
                                                                                     action:@selector(handlePan:)]
                                             autorelease];
    [self addGestureRecognizer:panRecognizer];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setupLayers];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupLayers];
}

- (void)dealloc {
    [_slideView removeFromSuperview];
    [_slideView release];
    [super dealloc];
}

- (void)layoutSubviews {
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)pushView:(UIView *)view withDirection:(SlideDirection)direction {
    UIView *superview = view.superview;
    [superview insertSubview:self belowSubview:view];
    
    CGPoint destPos = direction == LEFT_TO_RIGHT ? CGPointMake([UIScreen mainScreen].bounds.size.width - 40, 0) : CGPointMake(-([UIScreen mainScreen].bounds.size.width - 40), 0);
    [_slideView removeFromSuperview];
    [_slideView release];
    _slideView = [[SlideView alloc] initWithFrame:self.bounds
                                  snapshotForView:view
                                withSlidePosition:destPos
                                    slideFromLeft:direction == LEFT_TO_RIGHT];
    [self addSubview:_slideView];
    [_slideView animateSlideOut];
}

@end
