//
//  TimeLayer.m
//  PathDemo
//
//  Created by 劳中成 on 12-3-19.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "TimeLayer.h"

@implementation TimeLayer

@synthesize triangleSize = _triangleSize;

- (CGMutablePathRef)createHandPath:(CGFloat)handSz
{
    CGMutablePathRef handPath = CGPathCreateMutable();
    CGPathMoveToPoint(handPath, NULL, 6, 6);
    CGPathAddEllipseInRect(handPath, NULL, CGRectMake(0, 0, 3, 3));
    CGPathAddEllipseInRect(handPath, NULL, CGRectMake(0, 0, 1, 1));
    CGPathCloseSubpath(handPath);
    return handPath;
}

- (void)setupLayers {
    _shapeLayer = [CAShapeLayer layer];
    _shapeLayer.borderColor = [[UIColor blackColor] CGColor];
    _shapeLayer.shadowColor = [[UIColor blackColor] CGColor];
    _shapeLayer.shadowRadius = 1.0f;
    _shapeLayer.shadowOffset = CGSizeMake(0.5f, 0.5f);
    _shapeLayer.shadowOpacity = 0.9f;
    _shapeLayer.fillColor = [[UIColor blackColor] CGColor];
    _shapeLayer.opacity = 0.6f;
    [self addSublayer:_shapeLayer];
    
    _clockLayer = [CALayer layer];
    UIImage *clockImage = [UIImage imageNamed:@"feed-clock-face"];
    _clockLayer.contents = [clockImage CGImage];
    _clockLayer.bounds = CGRectMake(0, 0, [clockImage size].width, clockImage.size.height);
    [self addSublayer:_clockLayer];
    
    _clockCenterLayer = [CALayer layer];
    UIImage *clockCenterImage = [UIImage imageNamed:@"feed-clock-center"];
    _clockCenterLayer.contents = [clockCenterImage CGImage];
    _clockCenterLayer.bounds = CGRectMake(0, 0, clockCenterImage.size.width, clockCenterImage.size.height);
    [self addSublayer:_clockCenterLayer];

    _hourHandLayer = [CALayer layer];
    UIImage *hourHandImage = [UIImage imageNamed:@"feed-clock-hour-hand"];
    _hourHandLayer.contents = [hourHandImage CGImage];
    _hourHandLayer.bounds = CGRectMake(0, 0, hourHandImage.size.width, hourHandImage.size.height);
    
    _hourHandContainerLayer = [CALayer layer];
    _hourHandContainerLayer.bounds = _clockLayer.bounds;
    [_hourHandContainerLayer addSublayer:_hourHandLayer];
    [self addSublayer:_hourHandContainerLayer];

    _minuteHandLayer = [CALayer layer];
    UIImage *minuteHandImage = [UIImage imageNamed:@"feed-clock-minute-hand"];
    _minuteHandLayer.contents = [minuteHandImage CGImage];
    _minuteHandLayer.bounds = CGRectMake(0, 0, minuteHandImage.size.width, minuteHandImage.size.height);
    
    _minuteHandContainerLayer = [CALayer layer];
    _minuteHandContainerLayer.bounds = _clockLayer.bounds;
    [_minuteHandContainerLayer addSublayer:_minuteHandLayer];
    [self addSublayer:_minuteHandContainerLayer];
    
    // Time layers
    _timeTextEmbossLayer = [CATextLayer layer];
    _timeTextEmbossLayer.fontSize = 10;
    _timeTextEmbossLayer.foregroundColor = [[UIColor blackColor] CGColor];
    _timeTextEmbossLayer.contentsScale = [[UIScreen mainScreen] scale];
    [self addSublayer:_timeTextEmbossLayer];
    
    _timeTextLayer = [CATextLayer layer];
    _timeTextLayer.fontSize = _timeTextEmbossLayer.fontSize;
    _timeTextLayer.foregroundColor = [[UIColor whiteColor] CGColor];
    _timeTextLayer.contentsScale = [[UIScreen mainScreen] scale];
    [self addSublayer:_timeTextLayer];
    
    _dateTextEmbossLayer = [CATextLayer layer];
    _dateTextEmbossLayer.fontSize = 10;
    _dateTextEmbossLayer.foregroundColor = [[UIColor blackColor] CGColor];
    _dateTextEmbossLayer.contentsScale = [[UIScreen mainScreen] scale];
    [self addSublayer:_dateTextEmbossLayer];
    
    _dateTextLayer = [CATextLayer layer];
    _dateTextLayer.fontSize = _timeTextEmbossLayer.fontSize;
    _dateTextLayer.foregroundColor = [[UIColor lightGrayColor] CGColor];
    _dateTextLayer.contentsScale = [[UIScreen mainScreen] scale];
    [self addSublayer:_dateTextLayer];
}

- (id)init {
    self = [super init];
    if (self) {
        dispatch_queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,
                                                   0);
        _time = [[NSDate date] copy];
        _triangleSize = 10;
        [self setupLayers];
        [self setTime:_time];
    }
    return self;
}

- (void)dealloc {
    [_time release];
    [super dealloc];
}

- (void)setClockHandsPosition:(NSDate *)time {

    _hourHandContainerLayer.position = _clockLayer.position;
    _hourHandLayer.position = CGPointMake(_hourHandContainerLayer.position.x - _hourHandLayer.bounds.size.width / 2,
                                          _hourHandContainerLayer.position.y - _hourHandLayer.bounds.size.height);
    
    _minuteHandContainerLayer.position = _clockLayer.position;
    _minuteHandLayer.position = CGPointMake(_minuteHandContainerLayer.position.x - _minuteHandLayer.bounds.size.width / 2,
                                            _minuteHandContainerLayer.position.y - _minuteHandLayer.bounds.size.height + _clockCenterLayer.bounds.size.height / 2);

    NSDateComponents *components = [[NSCalendar currentCalendar] components:(NSHourCalendarUnit | NSMinuteCalendarUnit)
                                                                   fromDate:time];
    NSInteger hour = [components hour];
    NSInteger minute = [components minute];
    double hourDegree = (hour + minute / 60.0) / 12.0 * 360.0;
    double minuteDegree = minute / 60.0 * 360.0;
    
    CATransform3D hourTransform = CATransform3DMakeRotation(hourDegree / 180 * M_PI, 0, 0, 1.0);
    CATransform3D minuteTransform = CATransform3DMakeRotation(minuteDegree / 180 * M_PI, 0, 0, 1.0);
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform"];
    animation.fromValue = [NSValue valueWithCATransform3D:_hourHandContainerLayer.transform];
    animation.toValue = [NSValue valueWithCATransform3D:hourTransform];
    _hourHandContainerLayer.transform = hourTransform;
    [_hourHandContainerLayer addAnimation:animation forKey:@"hourTransform"];
    
    animation = [CABasicAnimation animationWithKeyPath:@"transform"];
    animation.fromValue = [NSValue valueWithCATransform3D:_minuteHandContainerLayer.transform];
    animation.toValue = [NSValue valueWithCATransform3D:minuteTransform];
    _minuteHandContainerLayer.transform = minuteTransform;
    [_minuteHandContainerLayer addAnimation:animation forKey:@"minuteTransform"];
}

- (void)updateLayerPositions {
    [self setClockHandsPosition:_time];
    
//    _timeTextEmbossLayer.backgroundColor = [[UIColor blueColor] CGColor];
    _timeTextEmbossLayer.bounds = CGRectMake(0, 0,
                                             self.bounds.size.width - _clockLayer.bounds.size.width - 2,
                                             (self.bounds.size.height - 8) / 2);
    
    _timeTextLayer.bounds = _timeTextEmbossLayer.bounds;
    _dateTextEmbossLayer.bounds = _timeTextEmbossLayer.bounds;
    _dateTextLayer.bounds = _dateTextEmbossLayer.bounds;

    NSDate *currentDate = [NSDate date];
    NSDateComponents *components = [[NSCalendar currentCalendar] components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit)
                                                                   fromDate:currentDate];
    NSDate *daystart = [[NSCalendar currentCalendar] dateFromComponents:components];
    BOOL showDate = [daystart compare:_time] == 1;
    if (showDate) {
        _timeTextEmbossLayer.position = CGPointMake(ceilf(_clockLayer.position.x + _clockLayer.bounds.size.width / 2 + _timeTextEmbossLayer.bounds.size.width / 2),
                                                    ceilf(_clockLayer.position.y - _timeTextEmbossLayer.bounds.size.height / 2 - 1));
        _timeTextLayer.position = CGPointMake(_timeTextEmbossLayer.position.x,
                                              _timeTextEmbossLayer.position.y + 1);
        _dateTextEmbossLayer.position = CGPointMake(_timeTextEmbossLayer.position.x,
                                                    _timeTextEmbossLayer.position.y + _timeTextEmbossLayer.bounds.size.height);
        _dateTextLayer.position = CGPointMake(_dateTextEmbossLayer.position.x,
                                              _dateTextEmbossLayer.position.y + 1);
        _dateTextEmbossLayer.opacity = 1.0f;
        _dateTextLayer.opacity = 1.0f;
        
    } else {
        _timeTextEmbossLayer.position = CGPointMake(ceilf(_clockLayer.position.x + _clockLayer.bounds.size.width / 2 + _timeTextEmbossLayer.bounds.size.width / 2),
                                                    _clockLayer.position.y);
        _timeTextLayer.position = CGPointMake(_timeTextEmbossLayer.position.x,
                                              _timeTextEmbossLayer.position.y + 1);
        _dateTextEmbossLayer.opacity = 0.0f;
        _dateTextLayer.opacity = 0.0f;
    }
}

- (void)layoutSublayers {
    _shapeLayer.bounds = self.bounds;
    _shapeLayer.position = CGPointMake(ceilf(self.bounds.size.width / 2),
                                       ceilf(self.bounds.size.height / 2));

    CGMutablePathRef path = CGPathCreateMutable();
    CGFloat radius = self.bounds.size.height / 2;
    CGPathAddArc(path, NULL, radius, radius, radius, M_PI_2, -M_PI_2, NO);
    CGPathAddLineToPoint(path, NULL, self.bounds.size.width - _triangleSize, 0);
    CGPathAddLineToPoint(path, NULL, self.bounds.size.width, self.bounds.size.height / 2);
    CGPathAddLineToPoint(path, NULL, self.bounds.size.width - _triangleSize, self.bounds.size.height);
    CGPathAddLineToPoint(path, NULL, radius, self.bounds.size.height);
    CGPathCloseSubpath(path);
    _shapeLayer.path = path;
    CFRelease(path);

    _clockLayer.position = CGPointMake(ceilf(_clockLayer.bounds.size.width / 2 + 2),
                                       _shapeLayer.position.y);
    _clockCenterLayer.position = _clockLayer.position;
    [self updateLayerPositions];
    
}

- (CGFloat)triangleSize {
    return _triangleSize;
}

- (void)setTriangleSize:(CGFloat)triangleSize {
    _triangleSize = triangleSize;
    [self setNeedsLayout];
}

- (void)layerFadeIn:(CALayer *)layer {
    if (layer.opacity != 1.0f) {
        [CATransaction begin];
        [CATransaction setDisableActions:YES];
        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
        animation.fromValue = [NSNumber numberWithFloat:layer.opacity];
        animation.toValue = [NSNumber numberWithFloat:1.0f];
        layer.opacity = 1.0f;
        [layer addAnimation:animation forKey:@"fadeIn"];
        [CATransaction commit];
    }
}

- (void)layerFadeOut:(CALayer *)layer {
    if (layer.opacity != 0.0f) {
        [CATransaction begin];
        [CATransaction setDisableActions:YES];
        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
        animation.fromValue = [NSNumber numberWithFloat:layer.opacity];
        animation.toValue = [NSNumber numberWithFloat:0.0f];
        layer.opacity = 0.0f;
        [layer addAnimation:animation forKey:@"fadeOut"];
        [CATransaction commit];
    }
}

- (void)show {
    [self layerFadeIn:self];
}

- (void)hide {
    [self layerFadeOut:self];
}

- (NSDate *)time {
    return [_time copy];
}

- (void)setTextLayerString {
    NSString *format = [NSDateFormatter dateFormatFromTemplate:@"hh:mm"
                                                       options:0
                                                        locale:[NSLocale currentLocale]];
    NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
    [formatter setDateFormat:format];
    
    NSString *formattedTime = [formatter stringFromDate:_time];
    _timeTextLayer.string = formattedTime;
    _timeTextEmbossLayer.string = formattedTime;
    
    format = [NSDateFormatter dateFormatFromTemplate:@"cc"
                                             options:0
                                              locale:[NSLocale currentLocale]];
    [formatter setDateFormat:format];
    formattedTime = [formatter stringFromDate:_time];
    _dateTextLayer.string = formattedTime;
    _dateTextEmbossLayer.string = formattedTime;
}

- (void)setTime:(NSDate *)time {
    if (![_time isEqualToDate:time]) {
        [_time release];
        _time = [time copy];
        [self setTextLayerString];
    }
}

+ (id)layer {
    return [[[TimeLayer alloc] init] autorelease];
}

@end
