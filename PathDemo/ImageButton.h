//
//  RequestButtonView.h
//  PathDemo
//
//  Created by 劳中成 on 12-3-18.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@class ImageButton;

@protocol ImageButtonDelegate <NSObject>

- (void)didButtonClicked:(ImageButton *)view;

@end

@interface ImageButton : UIView {
    UIView *_maskView;
    CALayer *_buttonContainerLayer;
    CALayer *_buttonLayer;
    CALayer *_buttonIndicatorLayer;
}

@property (nonatomic, readonly) BOOL checked;
@property (assign) id<ImageButtonDelegate> delegate;
@property (nonatomic, retain) UIImage *pressedImage;
@property (nonatomic, retain) UIImage *image;
@property (nonatomic, retain) UIImage *indicatorMaskImage;
@property (assign) CGFloat rotationDegree;

- (void)clearState;

@end
