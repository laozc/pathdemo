//
//  SideAttachedView.h
//  PathDemo
//
//  Created by 劳中成 on 12-3-18.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@class SlideView;

typedef enum {
    
    LEFT_TO_RIGHT,
    RIGHT_TO_LEFT
    
} SlideDirection;

@interface SnapshotSlideView : UIView {
    SlideView *_slideView;
    CGPoint _panLocation;
}

- (void)pushView:(UIView *)view withDirection:(SlideDirection)direction;

@end
