//
//  ViewController.m
//  PathDemo
//
//  Created by 劳中成 on 12-3-17.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ViewController.h"
#import "ConfigViewController.h"
#import "ConnectionViewController.h"
#import "ButtonHolder.h"

@implementation ViewController

@synthesize requestButton, configItem, connectionItem;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.requestButton.delegate = self;
    self.requestButton.image = [UIImage imageNamed:@"story-add-button"];
    self.requestButton.pressedImage = [UIImage imageNamed:@"story-add-button-pressed"];
    self.requestButton.indicatorMaskImage = [UIImage imageNamed:@"story-add-plus-pressed"];
    self.navigationItem.leftBarButtonItem = configItem;
    self.navigationItem.rightBarButtonItem = connectionItem;
    
    NSArray *images = [NSArray arrayWithObjects:
                       [NSArray arrayWithObjects:
                        @"story-button", @"story-button-pressed", @"story-camera", nil],
                       [NSArray arrayWithObjects:
                        @"story-button", @"story-button-pressed", @"story-people", nil],
                       [NSArray arrayWithObjects:
                        @"story-button", @"story-button-pressed", @"story-place", nil],
                       [NSArray arrayWithObjects:
                        @"story-button", @"story-button-pressed", @"story-music", nil],
                       [NSArray arrayWithObjects:
                        @"story-button", @"story-button-pressed", @"story-thought", nil],
                       [NSArray arrayWithObjects:
                        @"story-button", @"story-button-pressed", @"story-sleep", nil],
                       nil];
    NSMutableArray *buttons = [NSMutableArray array];
    
    CGFloat delay = 0.03;
    double degree = 90.0;
    double step = [images count] > 1 ? degree / ([images count] - 1) : 90;
    
    for (NSArray *imageArray in images) {
        UIImage *image = [UIImage imageNamed:[imageArray objectAtIndex:0]];
        ButtonHolder *holder = [[[ButtonHolder alloc] init] autorelease];
        
        CGPoint center = CGPointMake(ceilf(self.requestButton.frame.origin.x + self.requestButton.image.size.width / 2),
                                     ceilf(self.requestButton.frame.origin.y + (self.requestButton.image.size.height - image.size.height) / 2));
        holder.image = image;
        holder.pressedImage = [UIImage imageNamed:[imageArray objectAtIndex:1]];
        holder.indicatorMaskImage = [UIImage imageNamed:[imageArray objectAtIndex:2]];
        holder.centerPosition = center;
        holder.radius = 45.0;
        holder.bouncingRadius = 55.0;
        holder.degree = degree;
        holder.animationDelay = delay;
        holder.superview = self.view;
        [holder prepare];
        [buttons addObject:holder];
        
        delay += 0.025;
        degree -= step;
    }
    _storyButtons = [buttons copy];
    
    _historyViewController = [[HistoryViewController alloc] initWithNibName:@"HistoryViewController"
                                                                     bundle:nil];
    _historyViewController.view.frame = CGRectMake(0, 0,
                                                   _historyViewController.view.frame.size.width,
                                                   self.view.bounds.size.height);
    [self.view insertSubview:_historyViewController.view belowSubview:self.requestButton];
}

- (void)dealloc {
    [_storyButtons release];
    [_historyViewController release];
    [super dealloc];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.requestButton = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (CGPoint)getRequestButtonCenterPosition {
    return self.requestButton.layer.position;
}

- (void)clickOnMaskView {
    [self.requestButton clearState];
    for (ButtonHolder *holder in _storyButtons) {
        [holder animateHide:self.view below:self.requestButton];
        [holder clearState];
    }
    [_maskView removeFromSuperview];
    [_maskView release];
    _maskView = nil;
}

- (void)didButtonClicked:(ImageButton *)view {
    if (view.checked) {
        for (ButtonHolder *holder in _storyButtons) {
            [holder animateShow:self.view below:self.requestButton];
        }
        _maskView = [[MaskView alloc] initWithFrame:self.view.frame];
        UITapGestureRecognizer *tap = [[[UITapGestureRecognizer alloc] initWithTarget:self
                                                                               action:@selector(clickOnMaskView)]
                                       autorelease];
        [_maskView addGestureRecognizer:tap];
        [self.view insertSubview:_maskView belowSubview:self.requestButton];
        
    } else {
        [self clickOnMaskView];
    }
}

- (IBAction)pushConfigView:(id)sender {
    ConfigViewController *controller = [[[ConfigViewController alloc] initWithNibName:@"ConfigViewController"
                                                                               bundle:nil] autorelease];
    [controller animatePushIn:self];
}

- (IBAction)pushConnectionView:(id)sender {
    ConnectionViewController *controller = [[[ConnectionViewController alloc] initWithNibName:@"ConnectionViewController"
                                                                                       bundle:nil] autorelease];
    [controller animatePushIn:self];
}

@end
