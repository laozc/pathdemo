//
//  ButtonDescriptor.h
//  PathDemo
//
//  Created by 劳中成 on 12-3-19.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import "ImageButton.h"

@interface ButtonHolder : NSObject<ImageButtonDelegate> {
    ImageButton *_button;
    CALayer *_animationLayer;
    NSString *_animationIdentifier;
}

@property (nonatomic, retain) UIImage *pressedImage;
@property (nonatomic, retain) UIImage *image;
@property (nonatomic, retain) UIImage *indicatorMaskImage;
@property (nonatomic, assign) CGFloat radius;
@property (nonatomic, assign) CGFloat bouncingRadius;
@property (nonatomic, assign) CGFloat degree;
@property (nonatomic, assign) CGFloat animationDelay;
@property (nonatomic, assign) CGPoint centerPosition;
@property (nonatomic, retain) UIView *superview;
@property (nonatomic, assign) id<ImageButtonDelegate> delegate;

- (CGPoint)bouncingPosition;
- (CGPoint)position;
- (CGPoint)animationStartPosition;
- (void)prepare;
- (void)animateShow:(UIView *)view below:(UIView *)belowView;
- (void)animateHide:(UIView *)view below:(UIView *)belowView;
- (void)clearState;

@end
