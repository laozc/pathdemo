//
//  ButtonDescriptor.m
//  PathDemo
//
//  Created by 劳中成 on 12-3-19.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ButtonHolder.h"

@implementation ButtonHolder

@synthesize image, pressedImage, indicatorMaskImage, degree, animationDelay, centerPosition;
@synthesize radius, bouncingRadius, superview, delegate;

- (void)dealloc {
    self.image = nil;
    self.pressedImage = nil;
    self.indicatorMaskImage = nil;
    [_button release];
    [_animationLayer release];
    [super dealloc];    
}

- (CGPoint)getButtonOffset:(CGFloat)radius withDegree:(double)degree {
    CGFloat off = M_PI * radius;
    return CGPointMake(off * cos(degree * M_PI / 180),
                       off * sin((degree - 180) * M_PI / 180));
}

- (void)prepare {
    CGPoint offset = [self getButtonOffset:self.bouncingRadius withDegree:degree];
    [_button removeFromSuperview];
    [_button release];
    _button = [[ImageButton alloc] initWithFrame:CGRectMake(ceilf(self.centerPosition.x + offset.x),
                                                            ceilf(self.centerPosition.y + offset.y),
                                                            image.size.width,
                                                            image.size.height)];
    _button.rotationDegree = 0;
    _button.image = self.image;
    _button.pressedImage = self.pressedImage;
    _button.indicatorMaskImage = self.indicatorMaskImage;
    _button.delegate = self;
    
    _animationLayer = [[CALayer layer] retain];
//    _animationLayer.backgroundColor = [[UIColor greenColor] CGColor];
}

- (CGPoint)bouncingPosition {
    CGPoint offset = [self getButtonOffset:self.bouncingRadius withDegree:degree];
    return CGPointMake(ceilf(self.centerPosition.x + offset.x),
                       ceilf(self.centerPosition.y + offset.y));
}

- (CGPoint)position {
    CGPoint offset = [self getButtonOffset:self.radius withDegree:degree];
    return CGPointMake(ceilf(self.centerPosition.x + offset.x),
                       ceilf(self.centerPosition.y + offset.y));
}

- (CGPoint)animationStartPosition {
    return self.centerPosition;
}

- (void)animateShowButton:(ImageButton *)button
                   onView:(UIView *)view
                    below:(UIView *)belowView
               withDegree:(double)degree
                withDelay:(double)delay {
    
    CAAnimationGroup *anims = [CAAnimationGroup animation];
    CGPoint startPos = self.centerPosition;
    CGPoint endPos = self.position;
    CGPoint bouncingPos = self.bouncingPosition;
    
    _animationLayer.frame = CGRectMake(self.centerPosition.x,
                                       self.centerPosition.y,
                                       button.frame.size.width, button.frame.size.height);
    [_animationLayer addSublayer:button.layer];
    button.layer.position = CGPointMake(ceilf(button.bounds.size.width / 2),
                                        ceilf(button.bounds.size.height / 2));
    [view.layer insertSublayer:_animationLayer below:belowView.layer];

    CAKeyframeAnimation *posAnim = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    posAnim.values = [NSArray arrayWithObjects:
                      [NSValue valueWithCGPoint:startPos],
                      [NSValue valueWithCGPoint:startPos],
                      [NSValue valueWithCGPoint:endPos],
                      [NSValue valueWithCGPoint:bouncingPos],
                      [NSValue valueWithCGPoint:endPos],
                      nil];
    posAnim.keyTimes = [NSArray arrayWithObjects:
                        [NSNumber numberWithFloat:0.0],
                        [NSNumber numberWithFloat:0.1 + delay],
                        [NSNumber numberWithFloat:0.35 + delay],
                        [NSNumber numberWithFloat:0.5 + delay],
                        [NSNumber numberWithFloat:1.0 - delay],
                        nil];
    _animationLayer.position = endPos;
    
    CAKeyframeAnimation *anim = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    anim.values = [NSArray arrayWithObjects:
                   [NSValue valueWithCATransform3D:CATransform3DIdentity],
                   [NSValue valueWithCATransform3D:CATransform3DIdentity],
                   [NSValue valueWithCATransform3D:CATransform3DMakeRotation(M_PI_2, 0, 0, 1.0)],
                   [NSValue valueWithCATransform3D:CATransform3DMakeRotation(M_PI, 0, 0, 1.0)],
                   [NSValue valueWithCATransform3D:CATransform3DIdentity],
                   nil];
    anim.keyTimes = [NSArray arrayWithObjects:
                     [NSNumber numberWithFloat:0.0],
                     [NSNumber numberWithFloat:delay],
                     [NSNumber numberWithFloat:0.25 + delay],
                     [NSNumber numberWithFloat:0.55 + delay],
                     [NSNumber numberWithFloat:1.0 - delay],
                     nil];
    
    anims.duration = 0.5;
    anims.animations = [NSArray arrayWithObjects:posAnim, anim, nil];
    anims.delegate = self;
    _animationIdentifier = @"showButton";
    [_animationLayer addAnimation:anims forKey:_animationIdentifier];
}

- (void)animateHideButton:(ImageButton *)button
                   onView:(UIView *)view
                    below:(UIView *)belowView
               withDegree:(double)degree
                withDelay:(double)delay {
    
    [CATransaction begin];
    [CATransaction setDisableActions:YES];
    
    CAAnimationGroup *anims = [CAAnimationGroup animation];
    CGPoint startPos = self.centerPosition;
    CGPoint endPos = self.position;
    CGPoint bouncingPos = self.bouncingPosition;
    
    _animationLayer.frame = CGRectMake(endPos.x, endPos.y,
                                       button.frame.size.width, button.frame.size.height);
    [_animationLayer addSublayer:button.layer];
    button.layer.position = CGPointMake(ceilf(button.bounds.size.width / 2),
                                        ceilf(button.bounds.size.height / 2));
    [view.layer insertSublayer:_animationLayer below:belowView.layer];
    
    CAKeyframeAnimation *posAnim = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    posAnim.values = [NSArray arrayWithObjects:
                      [NSValue valueWithCGPoint:endPos],
                      [NSValue valueWithCGPoint:endPos],
                      [NSValue valueWithCGPoint:bouncingPos],
                      [NSValue valueWithCGPoint:endPos],
                      [NSValue valueWithCGPoint:startPos],
                      nil];
    posAnim.keyTimes = [NSArray arrayWithObjects:
                        [NSNumber numberWithFloat:0],
                        [NSNumber numberWithFloat:delay],
                        [NSNumber numberWithFloat:0.3 + delay],
                        [NSNumber numberWithFloat:0.5 + delay],
                        [NSNumber numberWithFloat:1.0 - delay],
                        nil];
    _animationLayer.position = startPos;
    
    CAKeyframeAnimation *anim = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    anim.values = [NSArray arrayWithObjects:
                   [NSValue valueWithCATransform3D:CATransform3DIdentity],
                   [NSValue valueWithCATransform3D:CATransform3DMakeRotation(M_PI, 0, 0, 1.0)],
                   [NSValue valueWithCATransform3D:CATransform3DMakeRotation(M_PI_2, 0, 0, 1.0)],
                   [NSValue valueWithCATransform3D:CATransform3DIdentity],
                   [NSValue valueWithCATransform3D:CATransform3DIdentity],
                   nil];
    anim.keyTimes = [NSArray arrayWithObjects:
                     [NSNumber numberWithFloat:0],
                     [NSNumber numberWithFloat:delay],
                     [NSNumber numberWithFloat:0.3 + delay],
                     [NSNumber numberWithFloat:0.5 + delay],
                     [NSNumber numberWithFloat:1.0 - delay],
                     nil];
    
    anims.duration = 0.5;
    anims.animations = [NSArray arrayWithObjects:posAnim, anim, nil];
    anims.delegate = self;
    _animationIdentifier = @"hideButton";
    [_animationLayer addAnimation:anims forKey:_animationIdentifier];
    [CATransaction commit];
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag {
    if (!flag) {
        return;
    }
    
    if ([@"hideButton" isEqualToString:_animationIdentifier]) {
        [_button removeFromSuperview];
        [_button.layer retain];
        [_button.layer removeFromSuperlayer];
        [_animationLayer removeFromSuperlayer];
        
    } else {
        [_button.layer retain];
        [_button.layer removeFromSuperlayer];
        _button.layer.position = self.position;//_buttonLayerPosition;
        [self.superview addSubview:_button];
        [_animationLayer removeFromSuperlayer];
    }
}

- (void)animateShow:(UIView *)view below:(UIView *)belowView {
    [self animateShowButton:_button
                     onView:view
                      below:belowView
                 withDegree:self.degree
                  withDelay:self.animationDelay];
}

- (void)animateHide:(UIView *)view below:(UIView *)belowView {
    [self animateHideButton:_button
                     onView:view
                      below:belowView
                 withDegree:self.degree
                  withDelay:self.animationDelay];
}

- (void)didButtonClicked:(ImageButton *)view {
    [self.delegate didButtonClicked:view];
}

- (void)clearState {
    [_button clearState];
}

@end
