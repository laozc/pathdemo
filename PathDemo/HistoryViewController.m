//
//  HistoryViewcontroller.m
//  PathDemo
//
//  Created by 劳中成 on 12-3-19.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "HistoryViewController.h"

#define CLOSE_TO_EDGE_GAP 10

@implementation HistoryViewController

@synthesize tableView, imageView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    self.tableView.delegate = self;
    [self.imageView setBackgroundImage:[UIImage imageNamed:@"mainFeed.jpg"]];

    _timeLayer = [TimeLayer layer];
    _timeLayer.bounds = CGRectMake(0, 0, 80, 28);
    
    _timeLayerContainer = [CALayer layer];
//    _timeLayerContainer.backgroundColor = [[UIColor colorWithWhite:0.5 alpha:0.5f] CGColor];
    _timeLayerContainer.frame = CGRectMake(self.view.bounds.size.width - 100, 0,
                                           80, self.view.bounds.size.height);
    [_timeLayerContainer addSublayer:_timeLayer];
    
    [self.view.layer addSublayer:_timeLayerContainer];
    _timeLayer.opacity = 0.0f;
    
    [self adjustTimeLayerPosition:self.tableView withTime:[NSDate date]];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.tableView = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *reuseIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:reuseIdentifier] autorelease];
    }
    
    cell.textLabel.text = @"This is only a test";
    return cell;
}

- (CGPoint)calculateTimeLayerPosition:(UIScrollView *)view
{
    CGPoint offset = [view contentOffset];
    CGFloat indicatorHeightOffset = 0;
    CGFloat indicatorOffset = 0;
    double indicatorRatio = view.bounds.size.height / view.contentSize.height;
    CGFloat fullIndicatorHeight = view.bounds.size.height * indicatorRatio;
    if (offset.y < 0) {
        indicatorHeightOffset = offset.y;
        indicatorOffset = 0;
        
    } else if (offset.y + view.bounds.size.height > view.contentSize.height) {
        indicatorHeightOffset = view.contentSize.height - view.bounds.size.height - offset.y;
        indicatorOffset = offset.y - (view.contentSize.height - view.bounds.size.height) * (1 - indicatorRatio);
        
    } else {
        indicatorHeightOffset = 0;
        indicatorOffset = offset.y * indicatorRatio;
    }
    CGFloat indicatorHeight = fullIndicatorHeight + indicatorHeightOffset;
    CGFloat y = ceilf((indicatorHeight) / 2 + indicatorOffset);
    if (y < _timeLayer.bounds.size.height / 2 + CLOSE_TO_EDGE_GAP) {
        y = self.view.bounds.size.height / 2 + CLOSE_TO_EDGE_GAP;
    }
    if (y + _timeLayer.bounds.size.height / 2 > self.view.bounds.size.height - CLOSE_TO_EDGE_GAP) {
        y = self.view.bounds.size.height - CLOSE_TO_EDGE_GAP - _timeLayer.bounds.size.height / 2;
    }
    return CGPointMake(_timeLayerContainer.bounds.size.width / 2, y);
}

- (void)adjustTimeLayerPosition:(UIScrollView *)view withTime:(NSDate *)time {
    [CATransaction begin];
    [CATransaction setDisableActions:YES];
    _timeLayer.position = [self calculateTimeLayerPosition:view];
    _timeLayer.time = time;
    [CATransaction commit];
}

- (void)hideTimeLayer {
    [_timeLayer hide];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGPoint offset = [scrollView contentOffset];
    CGFloat percentage = offset.y / scrollView.contentSize.height;
    NSDate *time;
    if (percentage < 0.3) {
        time = [NSDate dateWithTimeIntervalSinceReferenceDate:301000];
    } if (percentage > 0.3) {
        time = [NSDate dateWithTimeIntervalSinceReferenceDate:1000];
    }
    
    [NSTimer cancelPreviousPerformRequestsWithTarget:self
                                            selector:@selector(hideTimeLayer)
                                              object:nil];
    if (_timeLayer.opacity == 0.0f) {
        [_timeLayer show];
        [NSTimer scheduledTimerWithTimeInterval:2.0f
                                         target:self
                                       selector:@selector(hideTimeLayer)
                                       userInfo:nil
                                        repeats:FALSE];
    }
    
    [self adjustTimeLayerPosition:scrollView withTime:time];
    [self.imageView scroll:scrollView];
}

@end
