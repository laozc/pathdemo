//
//  ConnectViewController.h
//  PathDemo
//
//  Created by 劳中成 on 12-3-18.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "SnapshotSlideView.h"

@interface ConnectionViewController : UIViewController {
    UIViewController *_pushFrom;
}

@property (nonatomic, retain) IBOutlet SnapshotSlideView *slideView;

- (void)animatePushIn:(UIViewController *)controller;

@end
