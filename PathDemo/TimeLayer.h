//
//  TimeLayer.h
//  PathDemo
//
//  Created by 劳中成 on 12-3-19.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface TimeLayer : CALayer {
    CAShapeLayer *_shapeLayer;
    CALayer *_hourHandLayer;
    CALayer *_hourHandContainerLayer;
    CALayer *_minuteHandLayer;
    CALayer *_minuteHandContainerLayer;
    CALayer *_clockLayer;
    CALayer *_clockCenterLayer;
    CATextLayer *_timeTextLayer;
    CATextLayer *_timeTextEmbossLayer;
    CATextLayer *_dateTextLayer;
    CATextLayer *_dateTextEmbossLayer;
    dispatch_queue_t dispatch_queue;
    NSDate *_time;
}

@property (assign, nonatomic) CGFloat triangleSize;
@property (copy, nonatomic) NSDate *time;

- (void)show;
- (void)hide;

+ (id)layer;

@end
