//
//  BackgroundImageView.m
//  PathDemo
//
//  Created by 劳中成 on 12-3-17.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "BackgroundImageView.h"

@implementation BackgroundImageView

- (void)setupLayers {
    _contentLayer = [CALayer layer];
    _contentLayer.masksToBounds = YES;
    
    _backgroundImageLayer = [CALayer layer];
//    _backgroundImageLayer.backgroundColor = [[UIColor blueColor] CGColor];
    [_contentLayer addSublayer:_backgroundImageLayer];
    boundsSize = self.bounds.size;
    
    _imageGradientLayer = [CAGradientLayer layer];
    _imageGradientLayer.colors = [NSArray arrayWithObjects:
                                  [[UIColor colorWithWhite:0.0 alpha:0.0] CGColor],
                                  [[UIColor colorWithWhite:0.0 alpha:0.0] CGColor],
                                  [[UIColor blackColor] CGColor],
                                  nil];
    _imageGradientLayer.locations = [NSArray arrayWithObjects:
                                     [NSNumber numberWithFloat:0.0f],
                                     [NSNumber numberWithFloat:0.8f],
                                     [NSNumber numberWithFloat:1.0f],
                                     nil];
    _imageGradientLayer.opacity = 0.5f;
    [_contentLayer addSublayer:_imageGradientLayer];
    
    [self.layer insertSublayer:_contentLayer atIndex:0];
    self.backgroundColor = [UIColor blueColor];
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _disableLayouting = NO;
        [self setupLayers];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupLayers];
}

- (void)dealloc {
    [_image release];
    [super dealloc];
}

- (UIImage *)backgroundImage {
    return _image;
}

- (void)setImageLayerPosition:(CGFloat)heightOffset {
    [CATransaction begin];
    [CATransaction setDisableActions:YES];
    CGPoint pos = CGPointMake(_image.size.width / 2,
                              boundsSize.height - _image.size.height / 2);
    
    _contentLayer.position = CGPointMake(pos.x, pos.y + abs(heightOffset));
//    pos.y + (heightOffset ? abs(heightOffset) : 0)
    _backgroundImageLayer.position = CGPointMake(pos.x, _contentLayer.bounds.size.height / 2 + heightOffset * 2 + 100);
    _imageGradientLayer.position = CGPointMake(pos.x, _image.size.height / 2);
    [CATransaction commit];
}

- (void)layoutSublayersOfLayer:(CALayer *)layer {
    if (_disableLayouting) {
        return;
    }
    
    if (layer == self.layer) {
        [self setImageLayerPosition:0];
    }
}

- (void)setBackgroundImage:(UIImage *)backgroundImage {
    [_image release];
    _image = [backgroundImage retain];
    _backgroundImageLayer.contents = [_image CGImage];
    _contentLayer.bounds = CGRectMake(0, 0, _image.size.width, _image.size.height);
    _backgroundImageLayer.bounds = _contentLayer.bounds;
    _imageGradientLayer.bounds = _contentLayer.bounds;
    [self setNeedsLayout];
}

- (void)scroll:(UIScrollView *)scrollView {
    CGPoint offset = scrollView.contentOffset;
    _disableLayouting = YES;
    CGFloat actualOffset = 0;
    if (offset.y < 0) {
        actualOffset = offset.y / 4;
        
    } else {
        _disableLayouting = NO;
    }
    
    self.frame = CGRectMake(self.frame.origin.x, actualOffset,
                            self.bounds.size.width, boundsSize.height + abs(actualOffset));
    [self setImageLayerPosition:actualOffset];
}

@end
