//
//  HistoryViewcontroller.h
//  PathDemo
//
//  Created by 劳中成 on 12-3-19.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BackgroundImageView.h"
#import "TimeLayer.h"

@interface HistoryViewController : UIViewController<UITableViewDelegate, UITableViewDataSource> {
    TimeLayer *_timeLayer;
    CALayer *_timeLayerContainer;
}

@property (nonatomic, retain) IBOutlet UITableView *tableView;
@property (nonatomic, retain) IBOutlet BackgroundImageView *imageView;

- (void)adjustTimeLayerPosition:(UIScrollView *)view withTime:(NSDate *)time;

@end
