//
//  RequestButtonView.m
//  PathDemo
//
//  Created by 劳中成 on 12-3-18.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ImageButton.h"

@implementation ImageButton

@synthesize checked = _checked, delegate, image, pressedImage, indicatorMaskImage, rotationDegree = _rotationDegree;

- (CALayer *)setupButtonLayers {
    CALayer *layer = [CALayer layer];
    
    _buttonLayer = [[CALayer layer] retain];
    
    _buttonIndicatorLayer = [[CALayer layer] retain];
    _buttonIndicatorLayer.backgroundColor = [[UIColor whiteColor] CGColor];

    CALayer *maskLayer = [CALayer layer];
    _buttonIndicatorLayer.mask = maskLayer;
    
    [layer addSublayer:_buttonLayer];
    [layer addSublayer:_buttonIndicatorLayer];
    
    return layer;
}

- (void)setupLayers {
    self.backgroundColor = [UIColor clearColor];
    _rotationDegree = 45;
    
    _buttonContainerLayer = [[self setupButtonLayers] retain];
    _buttonContainerLayer.position = CGPointMake(ceilf(self.bounds.size.width / 2),
                                                 ceilf(self.bounds.size.height / 2));
    [self.layer insertSublayer:_buttonContainerLayer atIndex:0];
    
    [self addObserver:self forKeyPath:@"image" options:NSKeyValueObservingOptionNew context:nil];
    [self addObserver:self forKeyPath:@"pressedImage" options:NSKeyValueObservingOptionNew context:nil];
    [self addObserver:self forKeyPath:@"indicatorMaskImage" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)layoutSublayersOfLayer:(CALayer *)layer {
    _buttonLayer.frame = CGRectMake(0, 0, self.image.size.width, self.image.size.height);
    _buttonContainerLayer.bounds = _buttonLayer.bounds;
    
    _buttonIndicatorLayer.frame = CGRectMake((self.image.size.width - self.indicatorMaskImage.size.width) / 2,
                                             (self.image.size.height - self.indicatorMaskImage.size.height) / 2,
                                             self.indicatorMaskImage.size.width,
                                             self.indicatorMaskImage.size.height);
    _buttonIndicatorLayer.mask.bounds = _buttonIndicatorLayer.bounds;
    _buttonIndicatorLayer.mask.position = CGPointMake(ceilf(self.indicatorMaskImage.size.width / 2),
                                                      ceilf(self.indicatorMaskImage.size.height / 2));
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    [self.layer setNeedsLayout];
    if ([@"image" isEqualToString:keyPath] && !self.checked) {
        _buttonLayer.contents = [self.image CGImage];
        
    } else if ([@"pressedImage" isEqualToString:keyPath] && self.checked) {
        _buttonLayer.contents = [self.image CGImage];
        
    } else {
        _buttonIndicatorLayer.mask.contents = [self.indicatorMaskImage CGImage];
    }
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setupLayers];
    }
    return self;
}

- (void)dealloc {
    [self removeObserver:self forKeyPath:@"image"];
    [self removeObserver:self forKeyPath:@"pressedImage"];
    [self removeObserver:self forKeyPath:@"indicatorMaskImage"];
    [_buttonIndicatorLayer release];
    [_buttonLayer release];
    self.image = nil;
    self.pressedImage = nil;
    self.indicatorMaskImage = nil;
    [super dealloc];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupLayers];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    if ([_buttonContainerLayer hitTest:point] != nil) {
        return self;
    }
    return nil;
}

double radians(float degrees) {
    return (degrees * M_PI) / 180.0;
}

- (void)setState:(BOOL)pressed {
    if (pressed) {
        _buttonLayer.contents = [self.pressedImage CGImage];
        CGFloat v = 121.0/255.0;
        _buttonIndicatorLayer.backgroundColor = [[UIColor colorWithRed:v
                                                                 green:v
                                                                  blue:v
                                                                 alpha:1.0] CGColor];
        _buttonIndicatorLayer.transform = CATransform3DMakeRotation(radians(_rotationDegree), 0, 0, 1);

    } else {
        _buttonLayer.contents = [self.image CGImage];
        _buttonIndicatorLayer.backgroundColor = [[UIColor whiteColor] CGColor];
        _buttonIndicatorLayer.transform = CATransform3DIdentity;
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[[touches objectEnumerator] allObjects] objectAtIndex:0];
    if ([_buttonContainerLayer hitTest:[touch locationInView:self]] != nil) {
        [self setState:!_checked];
    }    
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[[touches objectEnumerator] allObjects] objectAtIndex:0];
    if ([_buttonContainerLayer hitTest:[touch locationInView:self]] != nil) {
        [self setState:!_checked];
    } else {
        [self setState:_checked];
    }
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    [self setState:self.checked];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[[touches objectEnumerator] allObjects] objectAtIndex:0];
    if ([_buttonContainerLayer hitTest:[touch locationInView:self]] != nil) {
        _checked = !_checked;
        [self setState:_checked];
        [delegate didButtonClicked:self];
    }
}

- (void)clearState {
    _checked = NO;
    [self setState:_checked];
}


@end
